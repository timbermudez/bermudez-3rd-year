import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(public route: Router) { }
  username: string;

  ngOnInit() {
  }
  login() {
    if (this.username.trim() === '') {
      return;
    }
    localStorage.setItem('username', this.username);
    this.route.navigateByUrl('/feed');
  }
}
