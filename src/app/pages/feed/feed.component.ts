import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { ChatService } from '../../chat.service';
import { interval } from 'rxjs';
import { startWith } from 'rxjs/operators';


@Component({
  selector: 'app-feed',
  templateUrl: './feed.component.html',
  styleUrls: ['./feed.component.scss']
})
export class FeedComponent implements OnInit {

  username: string;
  message: string;
  feeds: any;
  intervalObj: any;

  constructor(public route: Router, public chatSvc: ChatService) {
    if (localStorage.getItem('username') === null) {
      this.route.navigateByUrl('/login');
    }
    this.username = localStorage.getItem('username');
  }

  ngOnInit() {
    this.intervalObj = interval(3000).pipe(startWith(0))
    .subscribe(() => {
      this.getFeed();
    });
    this.getFeed();
  }

  postFeed() {
    this.chatSvc.postfeed(this.username, this.message)
    .subscribe(resp => {
      console.log(resp);
    });
  }

  getFeed() {
    this.chatSvc.getfeed().subscribe(resp => {
      this.feeds = resp;
      this.feeds.reverse();
    });
  }


}
